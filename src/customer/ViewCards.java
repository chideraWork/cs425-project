package customer;

import java.sql.ResultSet;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;

import sql.Jbc;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewCards extends JFrame{
String[] creditCards = new String[5];
JList list;
int custId;

	public ViewCards(int custId) {
		// TODO Auto-generated constructor stub
		this.custId = custId;
		loadCards();
		
		getContentPane().setLayout(null);
		
		JLabel lblYourCards = new JLabel("Your Cards");
		lblYourCards.setBounds(235, 13, 72, 16);
		getContentPane().add(lblYourCards);
		
			
		JButton btnAddNew = new JButton("Add New");
		btnAddNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				CreditCards frame = new CreditCards(custId);
		        frame.setTitle("My Credit Cards");
		        frame.setVisible(true);
		        frame.setBounds(10, 10, 810, 600);
		        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		        frame.setResizable(false);
			}
		});
		btnAddNew.setBounds(434, 25, 97, 25);
		getContentPane().add(btnAddNew);
		
		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int selectedIndex = list.getSelectedIndex();
				
				try {
	            	Jbc db = new Jbc();
	            	System.out.println(creditCards[selectedIndex]);
	            	db.writeData("Delete from credit_cards where CardNumber = "+ Integer.parseInt(creditCards[selectedIndex]));
	    		
			}catch(Exception e) {
				
			}
				creditCards[list.getSelectedIndex()] = "";
				list.getSelectionModel().removeIndexInterval(selectedIndex, selectedIndex);
			}
		});
		btnDelete.setBounds(435, 545, 97, 25);
		getContentPane().add(btnDelete);
		}
	
	void loadCards() {
		Jbc db = new Jbc();
		int i = 0;
		try {
			ResultSet rs = db.readData("Select * FROM credit_cards where CustomerID = "+custId);
	        while(rs.next()){
	        creditCards[i] =""+rs.getInt("CardNumber");
	        i++;
	        }
	        list = new JList(creditCards);	
	        list.setBounds(27, 63, 505, 469);
			getContentPane().add(list);
		} catch (Exception e1) {
			System.out.println(e1.toString());
		
		}
	}
}
