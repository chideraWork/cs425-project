package customer;

public class Address {
	private String strtName, city, state, strtNo, aptNo;
	private int zipCode, id;
	public Address(String strtName, String city, String state, int zipCode ) {
		this.strtName = strtName;
		this.city = city;
		this.state = state;
		this.zipCode = zipCode;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getstrtName() {
		return strtName;
	}
	public void setstrtName(String strtName) {
		this.strtName = strtName;
	}
	public int getZipCode() {
		return zipCode;
	}
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	public String getStrtNo() {
		return strtNo;
	}
	public void setStrtNo(String strtNo) {
		this.strtNo = strtNo;
	}
	public String getAptNo() {
		return aptNo;
	}
	public void setAptNo(String aptNo) {
		this.aptNo = aptNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}
