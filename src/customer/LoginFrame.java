package customer;
import javax.swing.*;

import sql.Jbc;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.sql.ResultSet;

public class LoginFrame extends JFrame implements ActionListener, MouseListener {

    Container container = getContentPane();
    JLabel userLabel = new JLabel("Username");
    JLabel passwordLabel = new JLabel("Password");
    JTextField userTextField = new JTextField();
    JPasswordField passwordField = new JPasswordField();
    JButton loginButton = new JButton("Login");
    JButton resetButton = new JButton("Reset");
    JCheckBox showPassword = new JCheckBox("Show Password");
    JLabel icon = new JLabel(new ImageIcon(LoginFrame.class.getResource("logo.png")));
    JLabel newUser = new JLabel("New user? Sign up here.");

    LoginFrame() {
    	getContentPane().setBackground(Color.WHITE);
        setLayoutManager();
        setLocationAndSize();
        addComponentsToContainer();
        addActionEvent();
   }

    public void setLayoutManager() {
        container.setLayout(null); 
    }

    public void setLocationAndSize() {
    	icon.setBounds(50, 10, 250, 250);
        userLabel.setBounds(50, 300, 100, 30);
        passwordLabel.setBounds(50, 370, 100, 30);
        userTextField.setBounds(150, 300, 150, 30);
        passwordField.setBounds(150, 370, 150, 30);
        showPassword.setBounds(150, 400, 150, 30);
        showPassword.setBackground(Color.WHITE);
        loginButton.setBounds(50, 450, 100, 30);
        resetButton.setBounds(200, 450, 100, 30);
        newUser.setBounds(120, 500, 150, 30);
        

    }

    public void addComponentsToContainer() {
        container.add(icon);
    	container.add(userLabel);
        container.add(passwordLabel);
        container.add(userTextField);
        container.add(passwordField);
        container.add(showPassword);
        container.add(loginButton);
        container.add(resetButton);
        container.add(newUser);
    }

    public void addActionEvent() {
        loginButton.addActionListener(this);
        resetButton.addActionListener(this);
        showPassword.addActionListener(this);
        newUser.addMouseListener(this);
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == loginButton) {
            String userText;
            String pwdText;
            userText = userTextField.getText();
            pwdText = passwordField.getText();          
            Boolean found = false;
			ResultSet login_table;
			int custId = 0;
			Jbc db = new Jbc();
			try {
				login_table = db.readData("Select * FROM Customer WHERE FirstName= '"+ userText+ "'");
		        
				while(login_table.next()){
			        System.out.println(login_table.getString("Password"));
			        	if(pwdText.contentEquals(login_table.getString("Password"))) {
			        		custId = login_table.getInt("CustomerID");
			        		found = true;
			        		
			        	}
			        }

				if(found) {
					this.dispose();
	            	openHome(custId);
				}
				else
				{
					JOptionPane.showMessageDialog(null,"Invalid Login Details","Login Error",JOptionPane.ERROR_MESSAGE);
					passwordField.setText(null);
					userTextField.setText(null);
				}
			       
			} catch (Exception e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null,"Invalid Login Details","Login Error",JOptionPane.ERROR_MESSAGE);
				passwordField.setText(null);
				userTextField.setText(null);
			
			}
        }
        if (e.getSource() == resetButton) {
            userTextField.setText("");
            passwordField.setText("");
        }
        if (e.getSource() == showPassword) {
            if (showPassword.isSelected()) {
                passwordField.setEchoChar((char) 0);
            } else {
                passwordField.setEchoChar('*');
            }


        }
    }
    
    void openHome(int username) {
    	HomeFrame frame = new HomeFrame(username);
        frame.setTitle("Home");
        frame.setVisible(true);
        frame.setBounds(10, 10, 800, 800);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
    }

	@Override
	public void mouseClicked(MouseEvent arg0) {
		// TODO Auto-generated method stub
		if(arg0.getSource() == newUser) {
			openSignUp();
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == newUser) {
			newUser.setForeground(Color.RED);
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == newUser) {
			newUser.setForeground(Color.BLACK);
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	void openSignUp() {
		SignUpFrame frame = new SignUpFrame();
        frame.setTitle("Sign Up");
        frame.setVisible(true);
        frame.setBounds(10, 10, 787, 806);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        
	}

}



